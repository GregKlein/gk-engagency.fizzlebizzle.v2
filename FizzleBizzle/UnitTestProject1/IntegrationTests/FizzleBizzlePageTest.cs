﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestProject1.IntegrationTests.PageObjects;

namespace UnitTestProject1.IntegrationTests
{
    [TestClass]
    public class FizzleBizzlePageTest : IntegrationTest
    {
        [TestMethod]
        public void FizzleBizzle_PageLoads()
        {
            //Arrange
            var fizzlePage = new FizzleBizzlePageObject(_driver);

            //Assert
            Assert.IsTrue(fizzlePage.ContentLoaded());
        }

        [TestMethod]
        public void FizzleBizzlePage_InvalidInput_ShowsValidationErrors()
        {
            //Arrange
            var fizzlePage = new FizzleBizzlePageObject(_driver);

            //Act
            fizzlePage.ClickGenerate();

            //Assert
            Assert.IsTrue(fizzlePage.ValidationErrorsDisplayed());
        }

        [TestMethod]
        public void FizzleBizzlePage_FizzBuzzInput_ShowsFizzBuzzResult()
        {
            Assert.Inconclusive("TODO: Implement this test.");
        }

        [TestMethod]
        public void FizzleBizzlePage_FizzBuzzBazzInput_ShowsFizzBuzzBazzResult()
        {
            Assert.Inconclusive("TODO: Implement this test.");
        }

        [TestMethod]
        public void FizzleBizzlePage_StartLessThanEnd_ShowsInvalidStartAndEndError()
        {
            Assert.Inconclusive("TODO: Implement this test.");
        }

        [TestMethod]
        public void FizzleBizzlePage_BazzWithMissingPredicate_ShowsMissingPredicateError()
        {
            Assert.Inconclusive("TODO: Implement this test.");
        }
    }
}
