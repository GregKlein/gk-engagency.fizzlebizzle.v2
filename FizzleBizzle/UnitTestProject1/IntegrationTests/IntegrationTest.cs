﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;

namespace UnitTestProject1.IntegrationTests
{
    [TestClass]
    public class IntegrationTest
    {
        protected IWebDriver _driver;

        [TestInitialize()]
        public void TestInitialize()
        {
            _driver = new PhantomJSDriver();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            _driver.Quit();
        }
    }
}
