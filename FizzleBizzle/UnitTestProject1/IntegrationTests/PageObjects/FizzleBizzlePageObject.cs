﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.IntegrationTests.PageObjects
{
    class FizzleBizzlePageObject : PageObject
    {
        #region Selectors
        private readonly string HeaderSelector = "h1";
        private readonly string ValidationMessageSelector = ".field-validation-error";
        private readonly string GenerateButtonSelector = ".fizzle-bizzle-submit";
        private readonly string HeaderText = "Fizzle Bizzle Test";
        #endregion

        private WebDriverWait wait;

        public FizzleBizzlePageObject(IWebDriver driver) : base(driver)
        {
            var baseUrl = ConfigurationManager.AppSettings["SeleniumBaseUrl"];
            _url = baseUrl + "/";

            wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            _driver.Navigate().GoToUrl(_url);
        }

        internal void ClickGenerate()
        {
            var generateButton = wait.Until(ExpectedConditions
                    .ElementIsVisible(By.CssSelector(GenerateButtonSelector)));
            generateButton.Click();
        }

        internal bool ContentLoaded()
        {
            try
            {
                var header = wait.Until(ExpectedConditions
                    .ElementIsVisible(By.CssSelector(HeaderSelector)));
                return header.Text == HeaderText;
            }
            catch (ElementNotVisibleException)
            {
                return false;
            }
        }

        internal bool ValidationErrorsDisplayed()
        {
            try
            {
                var validationMessage = wait.Until(ExpectedConditions
                    .ElementIsVisible(By.CssSelector(ValidationMessageSelector)));
                return validationMessage.Displayed;
            }
            catch (ElementNotVisibleException)
            {
                return false;
            }
        }
    }
}
