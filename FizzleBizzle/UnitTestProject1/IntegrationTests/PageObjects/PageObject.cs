﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.IntegrationTests.PageObjects
{
    class PageObject
    {
        protected string _url;
        protected string _fullUrl;
        protected IWebDriver _driver;

        public PageObject(IWebDriver driver)
        {
            _driver = driver;
        }
    }
}
