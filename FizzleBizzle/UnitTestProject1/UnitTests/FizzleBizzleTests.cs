﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using fb = FizzleBizzle.Models;
using FizzleBizzle.Models.Exceptions;

namespace UnitTestProject1
{
    [TestClass]
    public class FizzleBizzleTests
    {
        #region FizzBuzz
        [TestMethod]
        public void FizzBuzz_InvalidVariables_ThrowsException()
        {
            var fizzleBizzle = new fb.FizzleBizzleCalculator();

            Assert.ThrowsException<InvalidArgumentException>(() => {
                fizzleBizzle.FizzBuzz(10, 3);
            });
        }

        [TestMethod]
        public void FizzBuzz_ValidInputOneThroughFive_OutputsValidString()
        {
            var fizzleBizzle = new fb.FizzleBizzleCalculator() { Fizz = 3, Buzz = 5 };
            var result = fizzleBizzle.FizzBuzz(1, 5);
            var joinedResult = String.Join(",", result);

            Assert.AreEqual("1,2,Fizz,4,Buzz", joinedResult);
        }

        [TestMethod]
        public void FizzBuzz_ValidInputOneThroughFifteen_OutputsValidString()
        {
            var fizzleBizzle = new fb.FizzleBizzleCalculator() { Fizz = 3, Buzz = 5 };
            var result = fizzleBizzle.FizzBuzz(1, 15);
            var joinedResult = String.Join(",", result);

            Assert.AreEqual("1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,FizzBuzz", joinedResult);
        }

        [TestMethod]
        public void FizzBuzz_ValidInputOneNumber_OutputsValidString()
        {
            var fizzleBizzle = new fb.FizzleBizzleCalculator() { Fizz = 3, Buzz = 5 };
            var result = fizzleBizzle.FizzBuzz(15, 15);
            var joinedResult = String.Join(",", result);

            Assert.AreEqual("FizzBuzz", joinedResult);
        }

        [TestMethod]
        public void FizzBuzz_FizzAndBuzzAreEqual_OutputsValidString()
        {
            var fizzleBizzle = new fb.FizzleBizzleCalculator() { Fizz = 3, Buzz = 3 };

            Assert.Inconclusive("TODO: Implement this test.");
        }
        #endregion

        #region FizzBuzzBazz
        [TestMethod]
        public void FizzBuzzBazz_InvalidVariables_ThrowsException()
        {
            var fizzleBizzle = new fb.FizzleBizzleCalculator() { Fizz = 3, Buzz = 5 };

            Assert.ThrowsException<InvalidArgumentException>(() => {
                fizzleBizzle.FizzBuzzBazz(10, 3, (i) => { return true; });
            });
        }

        [TestMethod]
        public void FizzBuzzBazz_ValidInputOneThroughFive_OutputsValidString()
        {
            Assert.Inconclusive("TODO: Implement this test.");
        }

        [TestMethod]
        public void FizzBuzzBazz_ValidInputOneThroughFifteen_OutputsValidString()
        {
            var fizzleBizzle = new fb.FizzleBizzleCalculator() { Fizz = 2, Buzz = 3 };
            var result = fizzleBizzle.FizzBuzzBazz(1, 15, (i) => { return i > 6; });
            var joinedResult = String.Join(",", result);

            Assert.AreEqual("1,Fizz,Buzz,Fizz,5,FizzBuzz,7,Fizz,Buzz,Fizz,11,FizzBuzzBazz,13,Fizz,Buzz", joinedResult);
        }

        [TestMethod]
        public void FizzBuzzBazz_ValidInputOneNumber_OutputsValidString()
        {
            Assert.Inconclusive("TODO: Implement this test.");
        }

        [TestMethod]
        public void FizzBuzzBazz_FizzAndBuzzAreEqual_OutputsValidString()
        {
            Assert.Inconclusive("TODO: Implement this test.");
        }
        #endregion
    }
}
