﻿$(document).ready(function () {
    var toggleButton = $("#fizzle-bizzle-bazz-checkbox");

    var setFormBazzState = function () {
        var checked = (toggleButton.prop('checked'));
        var elements = $(".predicate-details");

        if (checked) {
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.remove("hide");
            }
        }
        else {
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.add("hide");
            }
        }
    }

    setFormBazzState();
    $("#fizzle-bizzle-bazz-checkbox").on("change", setFormBazzState);
});