﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using FizzleBizzle.Mappers;
using FizzleBizzle.Models;
using FizzleBizzle.Services;

namespace FizzleBizzle
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterInstance<IPredicateMapper>(new PredicateMapper());
            builder.RegisterType<FizzleBizzleCalculator>().As<IFizzleBizzle>().InstancePerRequest();
            builder.RegisterType<FizzBuzzBazzService>().As<IFizzBuzzBazzService>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
