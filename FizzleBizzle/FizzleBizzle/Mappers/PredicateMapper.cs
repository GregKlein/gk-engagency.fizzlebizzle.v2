﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FizzleBizzle.Models;
using FizzleBizzle.Models.Exceptions;

namespace FizzleBizzle.Mappers
{
    public class PredicateMapper : IPredicateMapper
    {
        private Dictionary<PredicateOption, Predicate<int>> predicateDict;

        public PredicateMapper()
        {
            predicateDict = new Dictionary<PredicateOption, Predicate<int>>();
            predicateDict.Add(PredicateOption.FizzBuzzGreaterThanSix, (x => x > 6));
            predicateDict.Add(PredicateOption.FizzBuzzLessThanSix, (x => x < 6));
            predicateDict.Add(PredicateOption.ReplaceAllFizzBuzz, (x => true));
        }

        public Predicate<int> Map(PredicateOption? option)
        {
            if (!option.HasValue)
            {
                return null;
            }

            if (!predicateDict.ContainsKey(option.Value))
            {
                throw new PredicateNotFoundException();
            }

            return predicateDict[option.Value];
        }
    }
}