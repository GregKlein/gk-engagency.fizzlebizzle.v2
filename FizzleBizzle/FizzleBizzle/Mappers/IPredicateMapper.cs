﻿using FizzleBizzle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzleBizzle.Mappers
{
    public interface IPredicateMapper
    {
        /// <returns>Returns the predicate associated with the predicate option.</returns>
        /// <exception cref="PredicateNotFoundException">Throws if no valid predicate exists.</exception>
        Predicate<int> Map(PredicateOption? option);
    }
}
