﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzleBizzle.Models
{
    public enum PredicateOption
    {
        [Display(Name = "FizzBuzz greater than six")]
        FizzBuzzGreaterThanSix,
        [Display(Name = "FizzBuzz less than six")]
        FizzBuzzLessThanSix,
        [Display(Name = "Replace all Fizzbuzz")]
        ReplaceAllFizzBuzz
    }
}