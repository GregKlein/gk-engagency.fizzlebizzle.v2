﻿using FizzleBizzle.Models.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzleBizzle.Models
{
    public class FizzleBizzleCalculator : IFizzleBizzle
    {
        public int Fizz { set; get; }
        public int Buzz { set; get; }

        private const string FizzString = "Fizz";
        private const string BuzzString = "Buzz";
        private const string BazzString = "Bazz";

        //This method is shared between FizzBuzz and FizzBuzzBazz.  It does the initial test for "Is it fizz or buzz or both?".
        protected string TestForFizzBuzz(int numberUnderTest)
        {
            var fizzBuzzString = "";

            if(numberUnderTest % Fizz == 0)
            {
                fizzBuzzString += FizzString;
            }

            if (numberUnderTest % Buzz == 0)
            {
                fizzBuzzString += BuzzString;
            }

            return fizzBuzzString;
        }

        protected void ValidateInputs(int start, int end)
        {
            if(end < start)
            {
                throw new InvalidArgumentException();
            }
        }

        public string[] FizzBuzz(int start, int end)
        {
            ValidateInputs(start, end);

            //Alternatively, a string array (string[]) could be used here instead of a List<string>. I prefer using a list here because List.Add is slightly more
            //readable than assigning directly to a location in the array.
            List<string> fizzBuzzList = new List<string>();

            for(int i = start; i <= end; i++)
            {
                var current = TestForFizzBuzz(i);

                if (String.IsNullOrEmpty(current))
                {
                    current = i.ToString();
                }

                fizzBuzzList.Add(current);
            }

            return fizzBuzzList.ToArray();
        }

        public string[] FizzBuzzBazz(int start, int end, Predicate<int> bazz)
        {
            ValidateInputs(start, end);

            List<string> fizzBuzzBazzList = new List<string>();

            for (int i = start; i <= end; i++)
            {
                var current = TestForFizzBuzz(i);

                if(current == (FizzString + BuzzString) && bazz(i))
                {
                    current += BazzString;
                }


                if (String.IsNullOrEmpty(current))
                {
                    current = i.ToString();
                }

                fizzBuzzBazzList.Add(current);
            }

            return fizzBuzzBazzList.ToArray();
        }
    }
}