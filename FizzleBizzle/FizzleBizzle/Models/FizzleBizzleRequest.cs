﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzleBizzle.Models
{
    public class FizzleBizzleRequest
    {
        [Required]
        [LessThanOrEqualTo("End")]
        public int Start { get; set; }
        [Required]
        [GreaterThanOrEqualTo("Start")]
        public int End { get; set; }

        [Required(ErrorMessage = "Please enter a value for Fizz.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than zero.")]
        public int FizzValue { get; set;}

        [Required(ErrorMessage = "Please enter a value for Buzz.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than zero.")]
        public int BuzzValue { get; set; }

        public bool BazzEnabled { get; set; }

        [RequiredIf("BazzEnabled", true, ErrorMessage = "Please select a predicate")]
        public PredicateOption? PredicateOption { get; set; }

        public string Result { get; set; }
    }
}