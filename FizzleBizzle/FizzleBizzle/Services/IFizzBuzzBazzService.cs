﻿using FizzleBizzle.Mappers;
using FizzleBizzle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzleBizzle.Services
{

    //This service acts as an abstraction layer for the IFizzleBizzle interface, so the controller doesn't have to call IFizzleBizzle directly.
    //This will make it easier in the future if we want to replace IFizzleBizzle with, for ex., a third party library.
    public interface IFizzBuzzBazzService
    {
        string ProcessRequest(int FizzValue, int BuzzValue, int Start, int End, bool BazzEnabled, Predicate<int> BazzPredicate);
    }
}
