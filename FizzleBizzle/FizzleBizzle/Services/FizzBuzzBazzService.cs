﻿using FizzleBizzle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzleBizzle.Services
{
    public class FizzBuzzBazzService : IFizzBuzzBazzService
    {
        private IFizzleBizzle _fizzleBizzle { get; set; }

        public FizzBuzzBazzService(IFizzleBizzle fizzleBizzle)
        {
            _fizzleBizzle = fizzleBizzle;
        }

        public string ProcessRequest(int FizzValue, int BuzzValue, int Start, int End, bool BazzEnabled, Predicate<int> BazzPredicate)
        {
            _fizzleBizzle.Fizz = FizzValue;
            _fizzleBizzle.Buzz = BuzzValue;
            string[] resultArray;

            if (BazzEnabled)
            {
                resultArray = _fizzleBizzle.FizzBuzzBazz(Start, End, BazzPredicate);
            }
            else
            {
                resultArray = _fizzleBizzle.FizzBuzz(Start, End);
            }

            return String.Join(", ", resultArray);
        }
    }
}