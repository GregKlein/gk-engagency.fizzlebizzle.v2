﻿using FizzleBizzle.Mappers;
using FizzleBizzle.Models;
using FizzleBizzle.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzleBizzle.Controllers
{
    public class FizzleBizzleController : Controller
    {
        private IPredicateMapper _predicateMapper { get; set; }
        private IFizzBuzzBazzService _fizzBuzzBazzService;


        public FizzleBizzleController(IPredicateMapper predicateMapper, IFizzBuzzBazzService fizzBuzzBazzService)
        {
            _predicateMapper = predicateMapper;
            _fizzBuzzBazzService = fizzBuzzBazzService;
        }

        // GET: FizzleBizzle
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FizzleBizzleRequest request)
        {
            if (!ModelState.IsValid)
            {
                return View(request);
            }

            request.Result = 
                _fizzBuzzBazzService.ProcessRequest(request.FizzValue, request.BuzzValue, request.Start, request.End, 
                                                    request.BazzEnabled, _predicateMapper.Map(request.PredicateOption));

            return View("Index", request);
        }
    }
}